
**Message** (RuleEndpoint and Rule)

- https://docs.kubeedge.io/zh/docs/developer/custom_message_deliver/
- https://docs.kubeedge.io/zh/docs/developer/message_topics/
- https://gitee.com/g-k8s/fk-kubeedge/blob/sam-custom/docs/proposals/cloud-edge-custom-message-design.md
- https://gitee.com/g-k8s/fk-kubeedge/blob/sam-custom/docs/proposals/reliable-message-delivery.md #At-Least-Once
- 
- router: 9443
- eventBus_mqtt: 1883/1884
- serviceBus: 9060

## Message总线通道

```bash
# On starting EventBus, it subscribes to these 5 topics:
1. "$hw/events/node/+/membership/get"
2. "$hw/events/device/+/state/update"
3. "$hw/events/device/+/twin/+"
4. "$hw/events/upload/#"
5. "SYS/dis/upload_records"
6. "$ke/events/+/device/data/update"

# vers
Alpha: v1.6 rest->eventbus, eventbus->rest
Beta: v1.7 rest->servicebus
GA: TBD
```

## 压测

TODO

单条10k/100k/10M: 频次tps xx条/s; 单条耗时<异步:rev-send>; 带宽占用; cpu/mem占用

- 1-rest2mqtt
- 2-mqtt2server
- 3-rest2svcBus

## Test

1)**ct-mosquitto** image: `eclipse-mosquitto:1.6.15` #2.0.15-openssl  
2)**sample-http-message** image: `server.k8s.local:18443/build/sample-http-message:latest`(上1层目录,imgbuild.sh构建并推送到内置Registry)

- 1-rest2mqtt(update:先删再增才生效)

```bash
# kc apply -f messageRoute #先得定义rule-ep; 再是rule: 不然发送卡住.
# rule重新定义: 直接apply不生效，需先删除；
# [root@(⎈|ctx-sa:default) messageRoute]$ 
git pull; kc delete -f 1-rest2mqtt.yml; kc apply -f 1-rest2mqtt.yml

# ct-mosquitto:
mosquitto_sub -t 'test' -d 

# send
SERVER_IP=10.18.0.2 #172.25.23.192
NODE=rge128-172-17-0-6-c820f.kedge
curl -X POST -d'{"message": "123"}' http://$SERVER_IP:9443/$NODE/default/topic/test1

# mosquitto-queneLogs(mem?)
/mosquitto # find
.
./log
./config
./config/mosquitto.conf
./data
```

![](./png/1-rest2mqtt.png)

- 2-mqtt2server(必须指定source的`node_name`)

```bash
# 
# targetResource: {"resource":"http://10.18.0.2:17080/message"}
  # URL=http://httpmessage-svc.kubeedge.svc.cluster.local:17080/message
  URL=http://10.18.0.2:17080/message
  curl -H "Content-Type: application/json" -X POST -d "{user_id: 123, coin:100, success:1, msg:OK! }" $URL
  # Hello world,/message!
  curl -X POST -d "ABC" $URL

# rule
sourceResource: {"topic": "test","node_name": "rge128-172-17-0-6-c820f.kedge"}

# mqtt
mosquitto_pub -t 'default/test' -d -m '{"edgemsg":"msgtocloud"}'

# restRev
2023-05-16T17:01:41.677564708+08:00 body is:  ABC 
2023-05-16T17:01:41.677606953+08:00 
2023-05-16T17:01:42.358105559+08:00 body is:  ABC 
2023-05-16T17:01:42.358143762+08:00 
2023-05-16T17:02:01.034197324+08:00 body is:  {"edgemsg":"msgtocloud"} 
2023-05-16T17:02:01.034244263+08:00 
2023-05-16T17:02:04.795675549+08:00 body is:  {"edgemsg":"msgtocloud"} 
2023-05-16T17:02:04.795735814+08:00
```

![](./png/2-mqtt2rest.png)

- 3-rest2svcBus

```yml
        nodeAffinity: #OR:多条件匹配,让Pod既可在Master同时可在指定边节点运行
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: kubeedge.io/edgemesh
                operator: Exists #DoesNotExist
            - matchExpressions: #OR: 多条,只要满足任何一条
              - key: node-role.kubernetes.io/master
                operator: Exists
```

```bash
# send
SERVER_IP=10.18.0.2 #172.25.23.192
NODE=rge128-172-17-0-6-c820f.kedge
curl -X POST -d'{"message": "123"}' http://$SERVER_IP:9443/$NODE/default/edgebus/message

```

![](./png/3-rest2edgeBus.png)
