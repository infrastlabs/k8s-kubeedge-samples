
**TODO**

- +param: 接收消息只显示大小
- 基于kube-client查取api信息
- web页: jq操作+ws回执

```bash
curl -H "Content-Type: application/json" -X POST -d "{user_id: 123, coin:100, success:1, msg:OK! }" http://localhost:17080/message

# kcmd-inner
[root@(⎈|ctx-sa:default) ~]$ curl -H "Content-Type: application/json" -X POST -d "{user_id: 123, coin:100, success:1, msg:OK! }" http://httpmessage-svc.kubeedge.svc.cluster.local:17080/message
Hello world,/message!
```

**Acorn**

- region
- computeclass: limits,toleraton,affinity

```bash
# ac-usage-httpmessage.md
# build+run
acorn run -n httpmessage --compute-class app=computeclass01 --region=local --target-namespace=ac01  -e ENV=VAL123 -l label.key=val11 -v /_ext:/_ext -p httpmessage:17080 -u .

# build> run
img=registry.cn-shenzhen.aliyuncs.com/infrastlabs/httpmessage
ac image/images
ac image rm $img
ac build -t $img .
acorn run -n httpmessage --compute-class app=computeclass01 --region=local --target-namespace=ac01  -e ENV=VAL123 -l label.key=val11 -v /_ext:/_ext -p httpmessage:17080 -o yaml $img #viewYaml
acorn run -n alpine-v313 --compute-class app=computeclass01 --region=local --target-namespace=ac02 -e ENV=VAL123 -l label.key=val11 -v /_ext:/_ext -p httpmessage:17080 -u registry.cn-shenzhen.aliyuncs.com/infrastlabs/httpmessage-2
# docker pull> run: #ERR: 只有两KB(其它manifest:未拉取)

# image err（无Acornfile）
acorn run -n alpine-v313 --compute-class app=computeclass01 --region=local --target-namespace=ac02 -e ENV=VAL123 -l label.key=val11 -v /_ext:/_ext -p httpmessage:17080 -u registry.cn-shenzhen.aliyuncs.com/infrasync/alpine:3.13.12

# publish(命令中的不生效)
#  新ac build的img>> 有svc,ingress的
kc get app -A -o yaml

# exec/logs
ac exec alpine sh
ac logs alpine
ac logs -f alpine

# aio: 一对多|undock
ac build --push --tag registry.cn-shenzhen.aliyuncs.com/infrastlabs/httpmessage-app-tag .
undock --all registry.cn-shenzhen.aliyuncs.com/infrastlabs/httpmessage-app-tag:latest ./dist
# Administrator@WIN-2208071245 MINGW64 /d/Development/Projects/_eeKedge/k8s-kubeedge-samples/golang_http_message/dist/linux_amd64 (cut1)
$ ls -F |grep -v "/$"
Acornfile
images.json
vcs.json

# images-ct
ac images -ac
ac images httpmessage -ac 
ac container

# publicsh
# - cmd: -p 不生效
# - Acornfile: `port: publish: "svcport:ctport/tcp"` OK; 生成两个svc
ac run -n httpmessage --compute-class app=computeclass01 --region=local -p 18082:18082/tcp --publish app:17080/tcp -u --publish-all .
```

**Items**

- App
  - ~~Get: index message~~
  - ~~Rev: print message~~
- **Clust元数据**
  - Get ns/rule
    - rest{node/ns/uri}> node/mqtt{topic}  [uri,topic]
    - rest/uriA> node/servicebus{host,port}/uriB
    - mqtt{ns/topic}> restUrl{http://a.com/bb} [node/topic,url]
  - Get k8s nodes
    - Send: node/ns/uriA_topic/uriB (selectNode,getRule)
    - Rev(mqttOnly): svcLB> msgRev{type, node, date, content} (getTopicDest_channelDefined)| edgeOffine: `mqttMsg> meta> sqlite?`
- ~~Build/Registry~~
- Deploy_yamlControl
  - ~~云端：地址需暴露，让边端推送~~；(rule:addr/uri注册)
  - 边端：获取接收topic(getTopicName) || 推送topic (rule:mqtt)
  - 通道：拥塞情况，Qos限流；(边端本地disk?)

```bash
  - rest发mqtt; rest发edgeBus
  - mqtt发rest:
    - <svc's address:static>
    - rule解耦?(cloud部署时的更新)
    - 统一走nginxStream`server.k8s.local:msgPort`
  - CloudApp消息网关：Qos, 消息统计/质量(先汇总,再分发)
  - Edge单台mqtt/serviceBus, 多应用订阅/分发；
  - eMesh
    - `edgeApp> eMesh> 直发emqx`?; cloudApp订阅
    - ~~`edgeApp> 总线> app转>emqx`~~?;

```