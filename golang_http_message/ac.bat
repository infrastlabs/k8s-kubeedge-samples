@REM 
@REM acorn install --image registry.cn-shenzhen.aliyuncs.com/infrastlabs/acorn:v0.7.0-alpha1

@REM cmds: build|run; logs|exec; rm
@REM type: dp/sts/ds; --replica
@REM type: svc,ep/ingress; pvc>> -p/-expose; ingress/service_lb
@REM drun: -o yaml/json
@REM skip: skip build, pure run
@REM 
acorn build -t httpmessage .
acorn project 
acorn offerings computeclass
acorn run -n httpmessage --compute-class app=computeclass01 --region=local --target-namespace=ac01 -p 18082:18082/tcp -u httpmessage
@REM image(deploy)|dir(build+deploy)
@REM acorn run -n httpmessage --compute-class app=computeclass01 --region=local -u .
@REM acorn run -n httpmessage --compute-class app=computeclass01 --region=local -o yaml .

@REM   -n, --name string               Name of app to create
@REM   -e, --env strings               Environment variables
@REM   -s, --secret strings            Bind an existing secret (ex: sec-name:app-secret)
@REM   -v, --volume stringArray        Bind an existing volume (ex: pvc-name:app-data)
@REM   -p, --publish strings           Publish port public:private (ex 81:80)
@REM   -l, --label strings             Add labels to the app and the resources it creates (ex k=v, containers:k=v)
@REM   -f, --file string               Name of the build file (default "./Acornfile")
@REM   -i, --dev                       Enable interactive dev mode: logs/status in the foreground and stop on exit
@REM   -o, --output string             Output without creating app (json, yaml)
@REM   -P, --publish-all               Publish all (true) or none (false)

pause