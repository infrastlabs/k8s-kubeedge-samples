#!/bin/bash
cur=$(cd "$(dirname "$0")"; pwd)
cd $cur
REPO=server.k8s.local:18443
img=$REPO/build/sample-http-message:v1.0.0

# certs/host(dind: set@host)
# sudo bash regcert.sh
# wget -qO- https://gitee.com/infrastlabs/k8s-kubeedge-iot/raw/dev/kedge/regcert.sh |sudo bash -
curl -fsSL -o- https://gitee.com/infrastlabs/k8s-kubeedge-iot/raw/dev/kedge/regcert.sh |sudo bash -
cat /etc/hosts |grep -v "server.k8s.local" > /tmp/etc-hosts
# echo "172.25.23.192 server.k8s.local" >> /tmp/etc-hosts
echo "10.210.31.12 server.k8s.local" >> /tmp/etc-hosts
cat /tmp/etc-hosts |sudo tee /etc/hosts
echo admin123 |docker login $REPO --username=admin --password-stdin

docker build -t $img .
docker push $img

