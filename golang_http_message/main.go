package main
// https://zhuanlan.zhihu.com/p/440441486

import (
 "fmt"
 "net/http"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("Hello World!\n"))
    fmt.Fprintln(w, "hello world!")
}
func healthHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "I'm alive!")
}
func messageHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello world,%s!", r.URL.Path)

	// https://zhuanlan.zhihu.com/p/578256742
	// curl -H "Content-Type: application/json" -X POST -d "{user_id: 123, coin:100, success:1, msg:OK! }" "http://localhost:8080/api/login"
	// 获取请求报文的内容长度
	len := r.ContentLength
	// 新建一个字节切片，长度与请求报文的内容长度相同
	body := make([]byte, len)
	// 读取 r 的请求主体，并将具体内容读入 body 中
	r.Body.Read(body)
	// 将字节切片内容写入相应报文
	fmt.Println("body is: ", string(body), "\n")
}


func main() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/health", healthHandler)
	http.HandleFunc("/message", messageHandler)
    http.ListenAndServe(":17080", nil)
}
