

cp from: k8s-kubeedge-iot's [tag tmark4-eMeshSpeed,dnsIPindn]

- randFile

```bash
[root@node194 t_randfile]# touch randfile.sh
[root@node194 t_randfile]# vi randfile.sh [root@node194 t_randfile]# sh randfile.sh r1.txt 100 Mfile_size=100100+0 records in
100+0 records out
104857600 bytes (105 MB) copied, 0.713351 s, 147 MB/s[root@node194 t_randfile]# ll -htotal 101M-rw-r--r-- 1 root root 100M Apr  3 11:05 r1.txt
-rw-r--r-- 1 root root 1.6K Apr  3 11:05 randfile.sh
[root@node194 t_randfile]# vi r1.txt 
[root@node194 t_randfile]# tar -zcvf r1.txt.tar.gz r1.txt 
r1.txt
[root@node194 t_randfile]# ll -h
total 201M
-rw-r--r-- 1 root root 100M Apr  3 11:05 r1.txt
-rw-r--r-- 1 root root 101M Apr  3 11:06 r1.txt.tar.gz
-rw-r--r-- 1 root root 1.6K Apr  3 11:05 randfile.sh
```

**eMeshLogs**

```bash
# [root@(⎈ |default:kubeedge) sts-nginx-svc-meshspeed]$ kc get po -owide
NAME                   READY   STATUS    RESTARTS        AGE     IP              NODE                             NOMINATED NODE   READINESS GATES
edgemesh-agent-466d4   1/1     Running   0               2d11h   172.0.0.1       rmbian-172-31-0-3-094d7.kedge    <none>           <none>
edgemesh-agent-btg6v   1/1     Running   1 (2d11h ago)   2d11h   172.25.23.194   172.25.23.194                    <none>           <none>
edgemesh-agent-wnmmq   1/1     Running   1 (37h ago)     37h     172.0.0.1       en-vm1-172-70-0-14-f0e5b.kedge   <none>           <none>

# tenVM节点实例：
# [root@(⎈ |default:kubeedge) sts-nginx-svc-meshspeed]$ kc logs -f edgemesh-agent-wnmmq 
I0401 22:05:13.157135       1 server.go:55] Version: v1.13.0-dirty
I0401 22:05:13.157398       1 server.go:89] [1] Prepare agent to run
...
...
I0401 22:49:54.193322       1 log.go:184] [INFO] 10.4.0.4:42229 - 19826 "AAAA IN qq.com. udp 24 false 512" NOERROR qr,rd,ra 92 0.019909378s
I0403 09:00:01.344169       1 proxier.go:895] "Opened iptables from-containers public port for service" servicePortName="kubesphere-system/ks-console:nginx" protocol=TCP nodePort=30021
I0403 09:00:01.357523       1 proxier.go:906] "Opened iptables from-host public port for service" servicePortName="kubesphere-system/ks-console:nginx" protocol=TCP nodePort=30021
I0403 09:00:01.360166       1 proxier.go:916] "Opened iptables from-non-local public port for service" servicePortName="kubesphere-system/ks-console:nginx" protocol=TCP nodePort=30021
I0403 12:01:09.420556       1 log.go:184] [INFO] 10.4.0.4:51779 - 61843 "A IN speed-svc.default.svc.cluster.local. udp 53 false 512" NOERROR qr,aa,rd 104 0.137962886s
I0403 12:01:09.420633       1 log.go:184] [INFO] 10.4.0.4:51779 - 62193 "AAAA IN speed-svc.default.svc.cluster.local. udp 53 false 512" NOERROR qr,aa,rd 146 0.123222525s
I0403 12:01:09.440492       1 loadbalancer.go:717] Dial legacy network between stat-go-app-1 - {tcp en-vm1-172-70-0-14-f0e5b.kedge 10.4.0.7:80}
E0403 12:01:09.469718       1 conn.go:53] "src close failed" err="close tcp 10.4.0.1:48550->10.4.0.7:80: use of closed network connection"
E0403 12:01:09.469769       1 conn.go:53] "src close failed" err="close tcp 169.254.96.16:37751->10.4.0.4:42417: use of closed network connection"
I0403 12:01:20.547111       1 log.go:184] [INFO] 10.4.0.4:44463 - 22500 "AAAA IN speed-svc.default.svc.cluster.local. udp 53 false 512" NOERROR qr,aa,rd 146 0.000115488s
I0403 12:01:20.547168       1 log.go:184] [INFO] 10.4.0.4:44463 - 22185 "A IN speed-svc.default.svc.cluster.local. udp 53 false 512" NOERROR qr,aa,rd 104 0.00002957s
I0403 12:01:20.679733       1 tunnel.go:231] Could not find peer rmbian-172-31-0-3-094d7.kedge in cache, auto generate peer info: {12D3KooWCVj9onBBqxDEAfAkctWWFTT1BbgkbbviKgZce984ehaV: []}
E0403 12:01:20.948573       1 loadbalancer.go:683] "Dial failed" err="get proxy stream from rmbian-172-31-0-3-094d7.kedge error: new stream between rmbian-172-31-0-3-094d7.kedge err: failed to find any peer in table"
I0403 12:01:20.948863       1 loadbalancer.go:717] Dial legacy network between stat-go-app-1 - {tcp en-vm1-172-70-0-14-f0e5b.kedge 10.4.0.7:80}
E0403 12:01:20.949009       1 conn.go:50] "dest close failed" err="close tcp 169.254.96.16:37751->10.4.0.4:39401: use of closed network connection"
E0403 12:01:20.949026       1 conn.go:53] "src close failed" err="close tcp 10.4.0.1:48552->10.4.0.7:80: use of closed network connection"
I0403 12:01:33.852801       1 log.go:184] [INFO] 10.4.0.4:59228 - 61778 "AAAA IN speed-svc.default.svc.cluster.local. udp 53 false 512" NOERROR qr,aa,rd 146 0.000151983s
I0403 12:01:33.852898       1 log.go:184] [INFO] 10.4.0.4:59228 - 61317 "A IN speed-svc.default.svc.cluster.local. udp 53 false 512" NOERROR qr,aa,rd 104 0.000040699s
E0403 12:01:34.103992       1 loadbalancer.go:683] "Dial failed" err="get proxy stream from rmbian-172-31-0-3-094d7.kedge error: new stream between rmbian-172-31-0-3-094d7.kedge err: failed to find any peer in table"
I0403 12:01:34.104304       1 loadbalancer.go:717] Dial legacy network between stat-go-app-1 - {tcp en-vm1-172-70-0-14-f0e5b.kedge 10.4.0.7:80}
E0403 12:01:34.104945       1 conn.go:53] "src close failed" err="close tcp 169.254.96.16:37751->10.4.0.4:34582: use of closed network connection"
E0403 12:01:34.104960       1 conn.go:53] "src close failed" err="close tcp 10.4.0.1:48556->10.4.0.7:80: use of closed network connection"

```


**wgetFile**

```bash
# nginx@hk1box
/etc/nginx/conf.d # cd /usr/share/nginx/html/
/usr/share/nginx/html # ll
sh: ll: not found
/usr/share/nginx/html # ls
50x.html    index.html
/usr/share/nginx/html # touch rfile.sh
/usr/share/nginx/html # vi rfile.sh 
/usr/share/nginx/html # sh rfile.sh ss.bin 10 M
file_size=10
10+0 records in
10+0 records out
/usr/share/nginx/html # ls -lh
total 10M    
-rw-r--r--    1 root     root         497 May 25  2022 50x.html
-rw-r--r--    1 root     root         615 May 25  2022 index.html
-rw-r--r--    1 root     root        1.6K Apr  3 14:08 rfile.sh
-rw-r--r--    1 root     root       10.0M Apr  3 14:09 ss.bin

# alpine@23.194 ##wget无进度条
[root@(⎈ |default:default) ~]$ kc exec -it alpine-ds-p7zpq sh
/ # wget -q -T3 -O - speed-svc:80
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
/ # 
/ # wget speed-svc/ss.bin
Connecting to speed-svc (10.43.42.216:80)
saving to 'ss.bin'
ss.bin                23% |********************************                                                                                                            | 2400k  0:01:05 ETA^C
/ # 
/ # wget speed-svc/ss.bin
Connecting to speed-svc (10.43.42.216:80)
wget: can\'t open 'ss.bin': File exists
/ # rm -f ss.bin 
/ # wget speed-svc/ss.bin
Connecting to speed-svc (10.43.42.216:80)
saving to 'ss.bin'
ss.bin                61% |*************************************************************************************                                                       | 6271k  0:00:32 ETA



# 23.194宿主机下载（wget显示下载速度）
# 404
[root@node194 ~]# wget 10.43.42.216/ss.bin
--2023-04-03 22:16:21--  http://10.43.42.216/ss.bin
Connecting to 10.43.42.216:80... connected.
HTTP request sent, awaiting response... 404 Not Found
2023-04-03 22:16:21 ERROR 404: Not Found.

[root@node194 ~]# curl 10.43.42.216
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>


# sts只留0号实例；
[root@(⎈ |default:default) ~]$ kc scale statefulset stat-go-app --replicas=1
statefulset.apps/stat-go-app scaled

# 再次下载
[root@node194 ~]# wget 10.43.42.216/ss.bin
--2023-04-03 22:18:23--  http://10.43.42.216/ss.bin
Connecting to 10.43.42.216:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 10485760 (10M) [application/octet-stream]
Saving to: ‘ss.bin’

 6% [========>                                                                                                                                          ] 695,944      137KB/s  eta 70s    ^C  #137KB/s （经代理转发?）
[root@node194 ~]# 
```